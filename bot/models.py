from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver
from django.contrib.gis.db import models as geomodels
from django.contrib.postgres.search import SearchVector, SearchQuery, SearchRank, TrigramSimilarity
from django.contrib.gis.geos import Point


class AddressManager(models.Manager):
    def search(self, search_text):
        search_vectors = SearchVector('address', weight='A', config='russian')
        search_query = SearchQuery(search_text, config='russian')
        search_rank = SearchRank(search_vectors, search_query)
        trigram_similarity = TrigramSimilarity('address', search_text)
        qs = (
            self.get_queryset()
            .annotate(rank=search_rank + trigram_similarity)
            .order_by('-rank')
        )
        return qs


class Store(geomodels.Model):

    lat = models.DecimalField(max_digits=9, decimal_places=6, blank=True, null=True)
    lon = models.DecimalField(max_digits=9, decimal_places=6, blank=True, null=True)
    store_location = geomodels.PointField(default='POINT(0.0 0.0)')
    is_opened = models.BooleanField(blank=True, null=True, default=True)
    address = models.CharField(max_length=200, blank=True)

    stores_by_addr = AddressManager()
    objects = models.Manager()

    def __str__(self):
        return self.address

    def save(self, *args, **kwargs):
        self.store_location = Point(x=self.lon, y=self.lat, srid=4326)
        super().save(*args, **kwargs)

    def plu_stock(self, plu_name):
        search_vector = (
                SearchVector('plu__plu_name', weight='A', config='russian')
                + SearchVector('plu__ui4', weight='B', config='russian')
                + SearchVector('plu__ui3', weight='C', config='russian')
                + SearchVector('plu__ui2', weight='D', config='russian')
        )
        search_query = SearchQuery(plu_name, config='pg_catalog.russian')
        search_rank = SearchRank(search_vector, search_query)
        plu_stocks = self.stock.annotate(rank=search_rank).values('plu__plu_name', 'stock').order_by('-rank')
        return plu_stocks


class Plu(models.Model):
    plu_name = models.CharField(max_length=100, blank=True)
    ui2 = models.CharField(max_length=100, blank=True)
    ui3 = models.CharField(max_length=100, blank=True)
    ui4 = models.CharField(max_length=100, blank=True)

    def __str__(self):
        return self.plu_name


class Stock(models.Model):
    plu = models.ForeignKey(Plu, on_delete=models.CASCADE)
    store = models.ForeignKey(Store, on_delete=models.CASCADE, related_name='stock')
    stock = models.DecimalField(max_digits=10, decimal_places=3, blank=True, null=True)


class Traffic(models.Model):
    store = models.ForeignKey(Store, on_delete=models.CASCADE)
    hour = models.DateTimeField()
    traffic = models.IntegerField(blank=True, null=True)


class Profile(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE)
    chat_id = models.IntegerField(blank=True, null=True, unique=True)
    favourite_store = models.ForeignKey(Store, on_delete=models.CASCADE, blank=True, null=True)

    def set_favourite_store(self, store_id):
        self.user.profile.favourite_store = Store.objects.get(pk=store_id)
        self.user.profile.save()

    def set_chat_id(self, chat_id):
        self.user.username = chat_id
        self.chat_id = chat_id
        self.user.save()
        self.save()


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
