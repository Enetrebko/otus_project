from telebot import types
import json


def main_menu():
    keyboard = types.ReplyKeyboardMarkup(row_width=1, resize_keyboard=True)
    keyboard.add(types.InlineKeyboardButton(text='Find store'))
    keyboard.add(types.InlineKeyboardButton(text='My store'))
    keyboard.add(types.InlineKeyboardButton(text='Find goods'))
    return keyboard


def go_find_store_menu():
    keyboard = types.ReplyKeyboardMarkup(row_width=1, resize_keyboard=True)
    keyboard.add(types.InlineKeyboardButton(text='Find store'))
    return keyboard


def choose_store_keyboards():
    keyboard = types.ReplyKeyboardMarkup(row_width=1, resize_keyboard=True)
    button_geo = types.KeyboardButton(text="Share location", request_location=True)
    keyboard.add(button_geo)
    keyboard_inline = types.InlineKeyboardMarkup()
    switch_button = types.InlineKeyboardButton(text="Find by address", switch_inline_query_current_chat="Address: ")
    keyboard_inline.row(switch_button)
    return keyboard, keyboard_inline


def save_store_keyboard(store_id: int):
    keyboard_inline = types.InlineKeyboardMarkup()
    keyboard_inline.row(
        types.InlineKeyboardButton("Save this store",
                                   callback_data=json.dumps({"command": "save_store", "data": str(store_id)}))
    )
    return keyboard_inline


def find_product_keyboard():
    keyboard = types.InlineKeyboardMarkup()
    switch_button = types.InlineKeyboardButton(text="Find by name", switch_inline_query_current_chat="Product: ")
    keyboard.add(switch_button)
    return keyboard
