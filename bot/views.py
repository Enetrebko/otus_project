from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
from bot.bot import *

@csrf_exempt
def webhook(request):
    if request.method != "POST":
        data = "Method not allowed"
        return JsonResponse(data, safe=False)
    data = types.Update.de_json(request.body.decode('UTF-8'))
    bot.process_new_updates([data])
    return JsonResponse({"message": "ok"}, safe=False)
