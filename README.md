# Telegram-бот GoForGroceries


### Что умеет

1. По геолокации пользователя определять ближайший магазин 
2. Искать магазин по адресу и товар по названию 
3. Для товара показывать остаток в магазине
4. Для магазина показывать загруженность на ближайший час


### Как работает

- Библиотека для работы с telegram bot api - pyTelegramBotAPI
- Веб фреймворк – Django
- База данных – Postgres


Данные о мвгвзинах и товарах предварительно загружены в базу


#### Webhook

Бот общается с серверами Telegram через webhook.

Для получения запросов создан временный домен через ngrok

Уствновка webhook-а выполняется запросом 
```shell
https://api.telegram.org/bot{TOKEN}/setWebhook?url={URL}/bot/webhooks/
```
где TOKEN - токен, полученный у BotFather при регистрации бота, 
URL - адрес, полученный от ngrok (пример: 'b01ff1b2f221.ngrok.io')

в ответ придет
```shell
{"ok":true,"result":true,"description":"Webhook was set"}
```

URL также прописывается в ALLOWED_HOSTS в настройках django


#### Full-text search

Для поиска по товарам и адресам используются функции django SearchQuery, SearchRank, SearchVector, TrigramSimilarity:

В Postgres предварительно создается расширение:
```shell
CREATE EXTENSION pg_trgm;
```

Кастомный менеджер в django для поиска по тексту:

```python
from django.contrib.postgres.search import SearchQuery, SearchRank, SearchVector, TrigramSimilarity,


class AddressManager(models.Manager):
    def search(self, search_text):
        search_vectors = SearchVector('address', weight='A', config='russian')
        search_query = SearchQuery(search_text, config='russian')
        search_rank = SearchRank(search_vectors, search_query)
        trigram_similarity = TrigramSimilarity('address', search_text)
        qs = (
            self.get_queryset()
            .annotate(rank=search_rank + trigram_similarity)
            .order_by('-rank')
        )
        return qs
```

Модель:

```python
class Store(geomodels.Model):

    ...
    stores_by_addr = AddressManager()
    objects = models.Manager()
```

Использование во вью:

```python
data = Store.stores_by_addr.search(query)
```

#### Inline-mode

Поиск магазина по адресу и товара по названию осушествляется в inline-режиме

Как это выглядит в боте:
 
<img src="/uploads/a752377ceee41f53a716fcb8bcb1d2b9/2020-08-25_19.30.11.jpg"  width="250">

Возможность работы в inline-режиме для бота добавляется командой /setinline в BotFather 

Код в django для поиска магазина по адресу:

```python
@bot.inline_handler(func=lambda query: 'Address:' in query.query)
def find_store_by_address(query):
    try:
        text = '&'.join(query.query.replace('Address: ', '').split(' ')).lower()
        data = Store.stores_by_addr.search(text).values('id', 'address')[:5]
        lines = [(row.get('address'), row.get('id')) for row in data]
        results = []
        for index, line in enumerate(lines):
            address = line[0]
            store_id = line[1]
            results.append(
                types.InlineQueryResultArticle(
                    id=str(index),
                    title=address,
                    input_message_content=types.InputTextMessageContent(message_text=address),
                    reply_markup=save_store_keyboard(store_id)
                )
            )
        bot.answer_inline_query(query.id, results, cache_time=1)
    except Exception as e:
        print(e)
```


#### Поиск по координатам

В django есть встроенные инструменты для раблоты с геоданными

```python
from django.contrib.gis.db import models as geomodels
from django.contrib.gis.geos import Point
```

Предварительно в Postgres создается расширение:
```shell
CREATE EXTENSION postgis;
```

Модель для магазина:

```python
class Store(geomodels.Model):

    lat = models.DecimalField(max_digits=9, decimal_places=6, blank=True, null=True)
    lon = models.DecimalField(max_digits=9, decimal_places=6, blank=True, null=True)
    store_location = geomodels.PointField(default='POINT(0.0 0.0)')
    ...
```
Координаты при сохранении преобразуются в PointField:
```python
    def save(self, *args, **kwargs):
        self.store_location = Point(x=self.lon, y=self.lat, srid=4326)
        super().save(*args, **kwargs)
```

В админке django магазин теперь отображается на карте:

<img src="/uploads/d8ee9e8c5f004bfa4e1d17cfae892f9d/Screenshot_2020-08-25_at_20.58.52.png"  width="800">


Поиск ближайшего магазина:

```python
from django.contrib.gis.db.models.functions import Distance

@bot.message_handler(content_types=["location"])
def accept_location(message: types.Message):
    lon = float(message.location.longitude)
    lat = float(message.location.latitude)
    user_location = Point(x=lon, y=lat, srid=4326)
    store = Store.objects.annotate(distance=Distance('store_location', user_location)).order_by('distance').first()
    bot.send_message(message.chat.id, store, reply_markup=save_store_keyboard(store.pk))
```


### Как установить

1) Склонировать репозиторий

```shell
git clone https://gitlab.com/Enetrebko/otus_project.git
```

2) Положить файл .env в корневую папку

Пример:
```
SECRET_KEY=secret

USERNAME=admin
EMAIL=admin@example.com
PASSWORD=adminpass

STATIC_ROOT=/static
MEDIA_ROOT=/media

DATABASE_NAME=telebot_db_prod
DATABASE_USER=storebot
DATABASE_PASSWORD=password
DATABASE_HOST=db
DATABASE_PORT=5432

HOST='b01ff1b2f221.ngrok.io'

DJANGO_SETTINGS_MODULE=storebot.settings.prod

TOKEN=1130919898:AAGVeXY5RsYTybGBAMjivXaEH2yYI-XQRuY
```

3) Запустить

```shell
docker-compose up --build
```

Админка будет доступна на localhost:8000/

Вход с USERNAME/PASSWORD, указанными в .env
