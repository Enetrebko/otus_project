from django.contrib.gis import admin
from .models import Store


admin.site.register(Store, admin.OSMGeoAdmin)