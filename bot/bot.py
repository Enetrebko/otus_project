from telebot import TeleBot
from bot.models import Store, Traffic
from django.contrib.auth.models import User
from django.contrib.gis.db.models.functions import Distance
from django.contrib.gis.geos import Point
from datetime import datetime, timedelta
from bot.keyboards import *
from django.conf import settings


bot = TeleBot(settings.TOKEN, threaded=False)


@bot.message_handler(commands=['start'])
def start(message: types.Message):
    user, created = User.objects.get_or_create(profile__chat_id=message.chat.id)
    if created:
        user.profile.set_chat_id(message.chat.id)
    bot.send_message(message.chat.id, "Let's go for groceries!", reply_markup=main_menu())


@bot.callback_query_handler(func=lambda call: json.loads(call.data).get("command") == 'save_store')
def save_store(call: types.CallbackQuery):
    chat_id = call.message.chat.id if call.message else call.from_user.id
    user = User.objects.get(profile__chat_id=chat_id)
    user.profile.set_favourite_store(json.loads(call.data).get("data"))
    if call.message:
        bot.edit_message_text(chat_id=chat_id, message_id=call.message.message_id, text='Store saved')
    elif call.inline_message_id:
        bot.edit_message_text(inline_message_id=call.inline_message_id, text='Store saved')
    bot.send_message(chat_id, "What's next?", reply_markup=main_menu())


@bot.message_handler(func=lambda message: message.text == 'Find store')
def find_store(message: types.Message):
    keyboard, keyboard_inline = choose_store_keyboards()
    bot.send_message(message.chat.id, "Please share your location", reply_markup=keyboard)
    bot.send_message(message.chat.id, "Or input address", reply_markup=keyboard_inline)


@bot.message_handler(func=lambda message: message.text == 'My store')
def my_store(message: types.Message):
    user = User.objects.get(profile__chat_id=message.chat.id)
    store = user.profile.favourite_store
    hour = datetime.now().replace(second=0, microsecond=0, minute=0) + timedelta(hours=1)
    traffic = Traffic.objects.filter(store_id=store.pk).filter(hour=hour).first() or 0
    if store:
        bot.send_message(message.chat.id, f"Your store: {store}")
        bot.send_message(message.chat.id, f"Traffic next hour: {traffic}", reply_markup=main_menu())
    else:
        bot.send_message(message.chat.id, "Let's find you store", reply_markup=go_find_store_menu())


@bot.message_handler(content_types=["location"])
def accept_location(message: types.Message):
    lon = float(message.location.longitude)
    lat = float(message.location.latitude)
    user_location = Point(x=lon, y=lat, srid=4326)
    store = Store.objects.annotate(distance=Distance('store_location', user_location)).order_by('distance').first()
    bot.send_message(message.chat.id, store, reply_markup=save_store_keyboard(store.pk))


@bot.message_handler(func=lambda message: message.text == 'Find goods')
def find_goods(message: types.Message):
    user = User.objects.get(profile__chat_id=message.chat.id)
    store = user.profile.favourite_store
    if store:
        bot.send_message(message.chat.id, "What product do you need?", reply_markup=find_product_keyboard())
    else:
        bot.send_message(message.chat.id, "Let's find you store", reply_markup=go_find_store_menu())


@bot.inline_handler(func=lambda query: 'Product:' in query.query)
def find_plu_by_name(query):
    try:
        user = User.objects.get(profile__chat_id=query.from_user.id)
        store = user.profile.favourite_store
        text = '&'.join(query.query.replace('Product: ', '').split(' ')).lower()
        plu_stocks = store.plu_stock(text)[:5]
        lines = ['\nStock: '.join([row.get('plu__plu_name'), str(row.get('stock'))]) for row in plu_stocks]
        results = []
        for index, line in enumerate(lines):
            results.append(
                types.InlineQueryResultArticle(
                    id=str(index),
                    title=line,
                    input_message_content=types.InputTextMessageContent(message_text=line),
                )
            )
        bot.answer_inline_query(query.id, results, cache_time=1)
    except Exception as e:
        print(e)


@bot.inline_handler(func=lambda query: 'Address:' in query.query)
def find_store_by_address(query):
    try:
        text = '&'.join(query.query.replace('Address: ', '').split(' ')).lower()
        data = Store.stores_by_addr.search(text).values('id', 'address')[:5]
        lines = [(row.get('address'), row.get('id')) for row in data]
        results = []
        for index, line in enumerate(lines):
            address = line[0]
            store_id = line[1]
            results.append(
                types.InlineQueryResultArticle(
                    id=str(index),
                    title=address,
                    input_message_content=types.InputTextMessageContent(message_text=address),
                    reply_markup=save_store_keyboard(store_id)
                )
            )
        bot.answer_inline_query(query.id, results, cache_time=1)
    except Exception as e:
        print(e)
